<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Config;

class GenerationConfig
{
    /**
     * If true collections will be created for array / assocs which are not top-level references
     *
     * @var bool
     */
    protected $addCustomCollections;


    /**
     * Create the configuration for the generator
     *
     * @param array $settings
     */
    public function __construct(array $settings = [])
    {
        $this->addCustomCollections = $settings['addCustomCollections'] ?? true;
    }

    /**
     * @return bool
     */
    public function isAddCustomCollections(): bool
    {
        return $this->addCustomCollections;
    }
}