<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp;

use OpenapiNextGeneration\GenerationHelperPhp\TargetDirectory;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGeneratorInterface;

/**
 * Creates the files the given entity generator produced
 */
class EntityBuilder
{
    /**
     * @var EntityGeneratorInterface
     */
    protected $entityGenerator;

    /**
     * @param EntityGeneratorInterface $entityGenerator
     */
    public function __construct(EntityGeneratorInterface $entityGenerator)
    {
        $this->entityGenerator = $entityGenerator;
    }

    /**
     * Builds the entities and creates the files
     *
     * @param array $patterns
     * @param string $targetNamespace
     * @param string|null $targetDirectory
     * @throws \Exception
     */
    public function buildEntities(array $patterns, string $targetNamespace, string $targetDirectory = null)
    {
        if ($targetDirectory === null) {
            $targetDirectory = TargetDirectory::getNamespaceDirectory($targetNamespace);
        }
        $targetDirectory = TargetDirectory::getCanonicalTargetDirectory($targetDirectory);

        $this->entityGenerator->processPatterns($patterns, $targetNamespace);
        $resultCollector = $this->entityGenerator->getResultCollector();

        foreach ($resultCollector->getGeneratedEntities() as $className => $generatedEntity) {
            $generatedEntitiesTargetDirectory = TargetDirectory::getCanonicalTargetDirectory(
                $targetDirectory . EntityGeneratorInterface::NAMESPACE_GENERATED_ENTITIES
            );
            $file = $generatedEntitiesTargetDirectory . $className . '.php';
            file_put_contents($file, $generatedEntity);
        }

        foreach ($resultCollector->getEntities() as $className => $entity) {
            $entitiesTargetDirectory = TargetDirectory::getCanonicalTargetDirectory(
                $targetDirectory . EntityGeneratorInterface::NAMESPACE_ENTITIES
            );
            $file = $entitiesTargetDirectory . $className . '.php';
            if (!is_file($file)) {
                file_put_contents($file, $entity);
            }
        }

        foreach ($resultCollector->getCollections() as $className => $collection) {
            $collectionsTargetDirectory = TargetDirectory::getCanonicalTargetDirectory(
                $targetDirectory . EntityGeneratorInterface::NAMESPACE_COLLECTIONS
            );
            $file = $collectionsTargetDirectory . $className . '.php';
            if (!is_file($file)) {
                file_put_contents($file, $collection);
            }
        }
    }
}