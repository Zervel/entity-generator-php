<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern;

use OpenapiNextGeneration\EntityGeneratorPhp\Generator\EntityGeneratorInterface;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AbstractContainerPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\ArrayPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AssocPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use PhpParser\Comment\Doc;
use PhpParser\Node\Stmt\Namespace_;
use SimpleCollection\AssocCollection;
use SimpleCollection\Entity\EntityArrayCollection;

class Collection extends AbstractPattern
{
    protected string $class;


    public function build(AbstractContainerPattern $pattern): Namespace_
    {
        $namespaceName = $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_COLLECTIONS;
        $className = $this->createCollectionName($pattern);
        $this->class = $namespaceName . '\\' . $className;

        $namespace = $this->builder->namespace($namespaceName);

        $class = $this->builder->class($className);
        if ($pattern instanceof ArrayPattern) {
            $parentClass = EntityArrayCollection::class;
        } elseif ($pattern instanceof AssocPattern) {
            $parentClass = AssocCollection::class;
        }
        $class->extend($this->useCollector->useClass($parentClass));
        $classDocBlock = $this->createClassDocblock($pattern);
        if ($classDocBlock instanceof Doc) {
            $class->setDocComment($classDocBlock);
        }

        $namespace->addStmts($this->useCollector->buildUseStatements());
        $namespace->addStmt($class);

        return $namespace->getNode();
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * Creates the class name for the custom collection
     */
    protected function createCollectionName(AbstractContainerPattern $pattern): string
    {
        $contentProperty = $pattern->getContentProperty();
        if ($contentProperty instanceof EntityPattern) {
            return $contentProperty->getClassName() . 'Collection';
        } else {
            return $pattern->getUpperCamelCaseName() . 'Collection';
        }
    }

    protected function createClassDocblock(AbstractContainerPattern $pattern): ?Doc
    {
        $contentProperty = $pattern->getContentProperty();
        if ($contentProperty instanceof EntityPattern) {
            $entity = $this->useCollector->useClass(
                $this->targetNamespace . '\\' . EntityGeneratorInterface::NAMESPACE_ENTITIES . '\\' . $contentProperty->getClassName()
            );
            return new Doc(
<<< DOCBLOCK
/**
 * @method $entity current()
 * @method $entity|false next()
 * @method $entity scNext()
 * @method $entity|false prev()
 * @method $entity|false scPrev()
 * @method $entity|false rewind()
 * @method $entity|false end()
 * @method $entity offsetGet(\$mOffset)
 * @method $entity get(\$mOffset, \$mDefault = null)
 * @method $entity seek(\$iOffset)
 * @method $entity seekToKey(\$mKey, bool \$bStrictMode = true)
 */
DOCBLOCK
            );
        } else {
            return null;
        }
    }
}