<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\GeneratedEntity;

use OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern\AbstractPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\AbstractContainerPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\ArrayPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\EntityPattern;
use OpenapiNextGeneration\OpenapiPatternMapperPhp\Pattern\PropertyPattern;
use PhpParser\Builder\Method;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ArrayItem;
use PhpParser\Node\Expr\Assign;
use PhpParser\Node\Stmt\Expression;
use PhpParser\Node\Stmt\If_;
use PhpParser\Node\Stmt\Return_;

class ToArray extends AbstractPattern
{
    /**
     * Creates the toArray() method
     */
    public function build(array $properties): Method
    {
        $toArray = $this->builder->method('toArray');
        $toArray->makePublic();
        $toArray->setReturnType('array');
        $toArray->addStmts($this->createToArrayBody($properties));

        return $toArray;
    }

    /**
     * Creates the body of the toArray() method
     */
    protected function createToArrayBody(array $properties): array
    {
        $statements = [];

        if ($this->isSimpleArrayReturnPossible($properties)) {
            $arrayItems = [];
            /* @var PropertyPattern $property */
            foreach ($properties as $property) {
                $arrayItems[] = new ArrayItem(
                    $this->createToArrayCall($property),
                    $this->builder->val($property->getPattern()->getName())
                );
            }
            $statements[] = new Return_(new Array_($arrayItems));
        } else {
            $return = $this->builder->var('return');
            $statements[] = new Expression(new Assign($return, $this->builder->val([])));

            /* @var PropertyPattern $property */
            foreach ($properties as $property) {
                $pattern = $property->getPattern();
                $statement = new Expression(new Assign(
                    new Expr\ArrayDimFetch($return, $this->builder->val($pattern->getName())),
                    $this->createToArrayCall($property)
                ));
                if ($this->isKeyOmittable($pattern)) {
                    $statement = new If_(
                        $this->builder->funcCall(
                            'isset',
                            [$this->builder->propertyFetch($this->builder->var('this'), $pattern->getLowerCamelCaseName())]
                        ),
                        [
                            'stmts' => [$statement]
                        ]
                    );
                }
                $statements[] = $statement;
            }

            $statements[] = new Return_($return);
        }

        return $statements;
    }

    protected function isSimpleArrayReturnPossible(array $properties): bool
    {
        foreach ($properties as $property) {
            if ($this->isKeyOmittable($property->getPattern())) {
                return false;
            }
        }
        return true;
    }

    protected function isKeyOmittable(PropertyPattern $pattern): bool
    {
        return !$pattern instanceof AbstractContainerPattern
            && !$pattern->isNullable()
            && !$pattern->isRequired()
            && $pattern->getDefault() === null;
    }

    /**
     * Expression that creates the value of one property when calling toArray()
     */
    protected function createToArrayCall(Property $property): Expr
    {
        $pattern = $property->getPattern();
        $getCall = $basicGetCall = $this->builder->propertyFetch(
            $this->builder->var('this'),
            $pattern->getLowerCamelCaseName()
        );

        if ($pattern instanceof AbstractContainerPattern) {
            $contentProperty = $pattern->getContentProperty();
            if (
                $pattern instanceof ArrayPattern
                && (
                    $contentProperty instanceof EntityPattern ||
                    $contentProperty instanceof AbstractContainerPattern
                )
            ) {
                $getCall = $this->builder->methodCall($getCall, 'toArray');
            } else {
                $getCall = $this->builder->methodCall($getCall, 'getAll');
            }
        } elseif ($pattern instanceof EntityPattern) {
            $getCall = $this->builder->methodCall($getCall, 'toArray');
        }

        if ($getCall === $basicGetCall || ($pattern->isRequired() && !$pattern->isNullable())) {
            return $getCall;
        } else {
            return new Expr\Ternary(
                $basicGetCall,
                $getCall,
                $this->builder->val(null)
            );
        }
    }
}