<?php

namespace OpenapiNextGeneration\EntityGeneratorPhp\Generator\Pattern;

use OpenapiNextGeneration\EntityGeneratorPhp\Config\GenerationConfig;
use OpenapiNextGeneration\EntityGeneratorPhp\Generator\UseCollector;
use PhpParser\BuilderFactory;

abstract class AbstractPattern
{
    protected GenerationConfig $config;
    protected string $targetNamespace;
    protected UseCollector $useCollector;
    protected BuilderFactory $builder;


    public function __construct(GenerationConfig $config, string $targetNamespace, UseCollector $useCollector = null)
    {
        $this->config = $config;
        $this->targetNamespace = $targetNamespace;
        $this->useCollector = $useCollector ?? new UseCollector();
        $this->builder = new BuilderFactory();
    }
}